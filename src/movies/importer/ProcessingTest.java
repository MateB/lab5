//Mate Barabas 1834578
package movies.importer;

import java.io.*;

public class ProcessingTest {

	public static void main(String[] args) throws IOException
	{
		String src = "C:\\Users\\1834578\\testInput";
		String dest = "C:\\Users\\1834578\\testDestination";
		LowerCaseProcessor lcp = new LowerCaseProcessor(src, dest);
		lcp.execute();
		
		String src2 = "C:\\Users\\1834578\\testDestination";
		String dest2 = "C:\\Users\\1834578\\testDestination";
		RemoveDuplicates rd = new RemoveDuplicates(src2, dest2);
		rd.execute();
		
	}

}
