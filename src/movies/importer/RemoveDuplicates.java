//Mate Barabas 1834578
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor
{
	public RemoveDuplicates(String src, String dest)
	{
		super(src, dest, false);
	}
	
	public ArrayList<String> process(ArrayList<String> a)
	{
		ArrayList<String> newList = new ArrayList<String>();
		
		for(String x : a)
		{
			if(a.contains(x))
			{
				newList.remove(x.toLowerCase());
			}
			newList.add(x.toLowerCase());
		}
		return newList;
	}
}
