//Mate Barabas 1834578
package movies.importer;
import java.util.*;
public class LowerCaseProcessor extends Processor
{
	
	public LowerCaseProcessor(String src, String dest)
	{
		super(src, dest, true);
	}
	
	public ArrayList<String> process(ArrayList<String> a)
	{
		ArrayList<String> asLower = new ArrayList<String>();
		
		for(String x : a)
		{
			asLower.add(x.toLowerCase());
		}
		return asLower;
	}
	
}
